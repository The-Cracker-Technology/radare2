chown -R andrax:andrax $(pwd)
chmod -R 755 $(pwd)

./configure

make uninstall

make purge

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Purge... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax $(pwd)
chmod -R 755 $(pwd)

sys/install.sh --without-syscapstone --with-capstone5 --install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "SYS/Install... PASS!"
else
  # houston we have a problem
  exit 1
fi

rm -rf /usr/local/lib/radare2/last/*.mk
rm -rf /usr/local/lib/radare2/last/*.js
rm -rf /usr/local/lib/radare2/last/*.c

